# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import socket
from trytond.model import ModelSQL, ModelView, fields, DeactivableMixin
from trytond.pyson import Eval
from trytond.config import parse_uri, get_hostname, get_port

__all__ = ['Bascule']


class Bascule(DeactivableMixin, ModelSQL, ModelView):
    '''Stock bascule'''
    __name__ = 'stock.bascule'

    name = fields.Char('Name', required=True, states={
            'readonly': ~Eval('active')},
        depends=['active'])
    model = fields.Many2One('ir.model', 'Model', required=True, states={
            'readonly': ~Eval('active')},
        depends=['active'], help='Determines what is weigh.')
    uri = fields.Char('Uri', required=True, states={
            'readonly': ~Eval('active')
        },
        depends=['active'],
        help='Determines the connection info (i.e. tcp://ip:port)')

    def read_measure(self):
        uri = parse_uri(self.uri)

        host = get_hostname(uri.netloc)
        port = get_port(uri.netloc)
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.settimeout(10)
            s.connect((host, port))
            s.send(b'GET')
            data = s.recv(1024).decode()
            s.close()
        return float(data) if data else None
