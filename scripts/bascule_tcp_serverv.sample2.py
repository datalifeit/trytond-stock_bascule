import socket
import serial

TCP_HOST = ''
TCP_PORT = 5000

COM_PORT = 'COM1'
COM_BAUD = 9600


def get_bascule_value():
    '''Read from serial'''
    with serial.Serial(COM_PORT, COM_BAUD, timeout=50,
            parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS) as conn:
        # need to send signal and get an stable measure
        value = '\x02!'
        while value[1] != 'I':
            conn.write(b'$')
            value = conn.read(12).decode('utf-8')

    print("[+] Reading from bascule: {0}".format(value[2:]))
    return value and value[2:-2].encode('utf-8') or b'0.0'


def server(host=TCP_HOST, port=TCP_PORT):
    '''Runs a listener tcp socker server'''
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind((host, port))
        sock.listen(10)
        print("[+] Listening on {0}:{1}".format(host, port))

        while True:
            conn, addr = sock.accept()
            print("[+] Connecting by {0}:{1}".format(addr[0], addr[1]))

            while True:
                request = conn.recv(4096)

                if not request:
                    print("[-] Close connection")
                    break

                print("[+] Received", repr(request.decode('utf-8')))

                response = get_bascule_value()
                conn.sendall(response)
                print("[+] Sending to {0}:{1}".format(addr[0], addr[1]))
            conn.close()


if __name__ == "__main__":
    server()
